﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Emgu.CV;
using Emgu.CV.Structure;

namespace ISIP_Algorithms.Tools
{
    public class Zero
    {
        public static Image<Gray,byte> Function(int width, int height)
        {
            Image<Gray, byte> Result = new Image<Gray, byte>(width,height);
            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                    Result.Data[y, x, 0] = (byte)(127.5+127.5*Math.Cos(3*Math.Pow(10,-4)*((x-383.5)* (x - 383.5)+(y-287.5)* (y - 287.5))));
            return Result;
        }

        public static Image<Gray, byte> Zero_Method(int width,int height, int treshold)
        {
            Image<Gray, byte> Poza_Generata = Function(width, height);
            Image<Gray, byte> Result = new Image<Gray, byte>(width, height);
            double[,] matrix = new double[height, width];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    matrix[i, j] = (double)(Poza_Generata.Data[i, j, 0] - 127.5);
                    //Result.Data[i, j, 0] = (byte)(matrix[i, j]);
                }
           
            for (int i = 1; i < height-1; i++)
                for (int j = 1; j < width-1; j++)
                    if ((matrix[i - 1, j - 1] >=  treshold && matrix[i + 1, j - 1] <= -treshold )||
                        (matrix[i - 1, j] >= treshold && matrix[i + 1, j] <= - treshold) ||
                        (matrix[i - 1, j + 1] >= treshold && matrix[i + 1, j+1] <= - treshold))
                        Result.Data[i, j, 0] = 255;
                    else if ((matrix[i - 1, j - 1] <= - treshold && matrix[i + 1, j - 1] >= treshold) ||
                        (matrix[i - 1, j] <= - treshold && matrix[i + 1, j] >= treshold) ||
                        (matrix[i - 1, j + 1] <= - treshold && matrix[i + 1, j+1] >= treshold))
                        Result.Data[i, j, 0] = 255;
                    else if ((matrix[i - 1, j - 1] <=treshold && matrix[i - 1, j + 1] >= - treshold) ||
                        (matrix[i, j - 1] <= treshold && matrix[i, j + 1] >= - treshold) ||
                        (matrix[i + 1, j - 1] <= treshold && matrix[i + 1, j + 1] >=  - treshold))
                        Result.Data[i, j, 0] = 255;
                    else if ((matrix[i - 1, j - 1] >= - treshold && matrix[i - 1, j + 1] <= treshold) ||
                        (matrix[i, j - 1] >= - treshold && matrix[i, j + 1] <= treshold) ||
                        (matrix[i + 1, j - 1] >= - treshold && matrix[i + 1, j + 1] <= treshold))
                        Result.Data[i, j, 0] = 255;
                    else
                        Result.Data[i, j, 0] = 0;
            return Result;
        }
    }
}
