﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Controls;

using Emgu.CV;
using Emgu.CV.Structure;
using ISIP_FrameworkHelpers;

namespace ISIP_Algorithms.Tools
{
    public class Rectangle_Function
    {

        public static Image<Gray, byte> Rectangle_Method(Image<Gray, byte> InputImage, out List<System.Windows.Point> maxime,Canvas canvas)
        {
            int max = 0;
            maxime = new List<System.Windows.Point>();
            Image<Gray, byte> Result = new Image<Gray, byte>(InputImage.Size);
            Image<Gray, byte> Reprezentare = new Image<Gray, byte>(181, 2 * (int)(Math.Sqrt(InputImage.Height * InputImage.Height + InputImage.Width * InputImage.Width)+0.5));
            Image<Gray, int> Hough = new Image<Gray, int>(181, 2 * (int)(Math.Sqrt(InputImage.Height * InputImage.Height + InputImage.Width * InputImage.Width)+0.5));
            int val = (int)(Math.Sqrt(InputImage.Height * InputImage.Height + InputImage.Width * InputImage.Width)+0.5);
            for (int y = 0; y < InputImage.Height; y++)
            {
                for (int x = 0; x < InputImage.Width; x++)
                {
                    double r;
                    if (InputImage.Data[y, x, 0] == 255)
                        for (int alfa = -90; alfa <= 90; alfa++)
                        {
                            double unghi = (Math.PI / 180 * alfa);
                            r = x * Math.Cos(unghi) + y * Math.Sin(unghi);
                            int coord = (int)(r + 0.5) + val;
                            Hough.Data[coord, alfa+90, 0]++;
                            if (Hough.Data[coord, alfa+90, 0] > max)
                                max = Hough.Data[coord, alfa+90, 0];
                        }
                }
            }

            for (int y = 0; y < Hough.Height; y++)
                for (int x = 0; x < Hough.Width; x++)
                    Reprezentare.Data[y, x, 0] = (byte)((255.0 / max)*Hough.Data[y, x, 0]);

            int T = 70;

            for (int y = 20; y < Reprezentare.Height - 20; y++)
            {
                for (int x = 20; x < Reprezentare.Width - 20; x++)
                {
                    max = 0;
                    for (int i = -20; i <= 20; i++)
                    {
                        for (int j = -20; j <= 20; j++)
                        {
                            if (Reprezentare.Data[y + i, x + j, 0] > max)
                                max = Reprezentare.Data[y + i, x + j, 0];
                        }
                    }
                    for (int i = -20; i <= 20; i++)
                    {
                        for (int j = -20; j <= 20; j++)
                        {
                            if (Reprezentare.Data[y + i, x + j, 0] < max)
                                Reprezentare.Data[y + i, x + j, 0] = 0;
                        }
                    }
                }
            }
            for (int y = 0; y < Reprezentare.Height; y++)
            {
                for (int x = 0; x < Reprezentare.Width; x++)
                {
                    if (Reprezentare.Data[y, x, 0] > 130)
                    {
                        int ynou = y - val;
                        int x1 = 0;
                        int x2 = InputImage.Width - 1;
                        int y1;
                        int y2;
                        if (x - 90 == 0 || x - 90 == 180)
                        {
                            y1 = ynou;
                            y2 = ynou;
                        }
                        else
                        {
                            y1 = (int)(ynou / Math.Sin(Math.PI / 180 * (x - 90)));
                            y2 = (int)((ynou - x2 * Math.Cos(Math.PI / 180 * (x - 90))) / Math.Sin(Math.PI / 180 * (x - 90)));
                        }
                        Point p1 = new Point(x1, y1);
                        Point p2 = new Point(x2, y2);
                        DrawHelper.DrawAndGetLine(canvas, p1, p2, Brushes.Blue);
                    }
                }
            }
                    

            return Reprezentare;
        }
    }
}