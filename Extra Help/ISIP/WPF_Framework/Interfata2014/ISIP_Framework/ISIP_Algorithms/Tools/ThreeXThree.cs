﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Emgu.CV;
using Emgu.CV.Structure;

namespace ISIP_Algorithms.Tools
{
    public class ThreeXThree
    {
        public static Image<Gray, byte> Three_Method(Image<Gray, byte> InputImage)
        {
            Image<Gray, byte> Result = new Image<Gray, byte>(InputImage.Size);
            for (int y = 0; y < InputImage.Height; y++)
            {
                for (int x = 0; x < InputImage.Width; x++)
                {
                    if (x == 0 || y == 0 || x == InputImage.Width - 1 || y == InputImage.Height - 1)
                        Result.Data[y, x, 0] = InputImage.Data[y, x, 0];
                    else
                        Result.Data[y, x, 0] = (byte)((int)((InputImage.Data[y, x, 0] + InputImage.Data[y, x - 1, 0] + 
                            InputImage.Data[y - 1, x - 1, 0] + InputImage.Data[y - 1, x, 0] + 
                            InputImage.Data[y - 1, x + 1, 0] + InputImage.Data[y, x + 1, 0] + 
                            InputImage.Data[y + 1, x + 1, 0] + InputImage.Data[y + 1, x, 0] + InputImage.Data[y + 1, x - 1, 0])/9));
                }
            }
            return Result;
        }
    }
}
