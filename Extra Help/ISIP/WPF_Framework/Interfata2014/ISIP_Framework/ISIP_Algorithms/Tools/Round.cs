﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Emgu.CV;
using Emgu.CV.Structure;

namespace ISIP_Algorithms.Tools
{
    public class Round
    {
        public static Image<Gray, byte> Round_Method(Image<Gray, byte> InputImage)
        {
            Image<Gray, byte> Result = new Image<Gray, byte>(InputImage.Size);
            int[] LookUp_Table = new int[256];
            double a = -2.0 / (255 * 255);
            double b = 3.0 / 255;
            for (int i = 0; i <= 255; i++)
                LookUp_Table[i] = (int)(a * Math.Pow(i, 3) + b * Math.Pow(i, 2) + 0.5);
            for (int y = 0; y < InputImage.Height; y++)
            {
                for (int x = 0; x < InputImage.Width; x++)
                {
                    if (x == 0 || y == 0 || x == InputImage.Width - 1 || 
                        y == InputImage.Height - 1 || x==1 || y==1 || 
                        x== InputImage.Width - 2 || y== InputImage.Height - 2)
                        Result.Data[y, x, 0] = InputImage.Data[y, x, 0];
                    else
                        Result.Data[y, x, 0] = (byte)((int)((InputImage.Data[y, x, 0] + InputImage.Data[y, x - 1, 0] +
                            InputImage.Data[y - 1, x - 1, 0] + InputImage.Data[y - 1, x, 0] +
                            InputImage.Data[y - 1, x + 1, 0] + InputImage.Data[y, x + 1, 0] +
                            InputImage.Data[y + 1, x + 1, 0] + InputImage.Data[y + 1, x, 0] +
                            InputImage.Data[y + 1, x - 1, 0] + InputImage.Data[y - 2, x - 1, 0] +
                            InputImage.Data[y - 2, x, 0] + InputImage.Data[y - 2, x + 1, 0] +
                            InputImage.Data[y - 1, x + 2, 0] + InputImage.Data[y, x + 2, 0] +
                            InputImage.Data[y + 1, x + 2, 0] + InputImage.Data[y - 1, x - 2, 0] +
                            InputImage.Data[y, x - 2, 0] + InputImage.Data[y + 1, x - 2, 0] +
                            InputImage.Data[y + 2, x - 1, 0] + InputImage.Data[y + 2, x, 0] +
                            InputImage.Data[y + 2, x + 1, 0]) / 21));
                }
            }
            return Result;
        }
    }
}
