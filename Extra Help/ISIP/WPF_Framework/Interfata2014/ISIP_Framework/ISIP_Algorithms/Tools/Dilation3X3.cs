﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Emgu.CV;
using Emgu.CV.Structure;

namespace ISIP_Algorithms.Tools
{
        public class Dilation3X3
        {
            public static Image<Gray, byte> Dilation3X3_Method(Image<Gray, byte> InputImage)
            {
                Image<Gray, byte> Result = new Image<Gray, byte>(InputImage.Size);

                for (int y = 1; y < InputImage.Height-1; y++)
                {
                    for (int x = 1; x < InputImage.Width-1; x++)
                    {
                        if (InputImage.Data[y, x, 0] == 255 || InputImage.Data[y - 1, x - 1, 0] == 255 || 
                        InputImage.Data[y - 1, x, 0] == 255 || InputImage.Data[y - 1, x + 1, 0] == 255 || 
                        InputImage.Data[y, x + 1, 0] == 255 || InputImage.Data[y + 1, x + 1, 0] == 255 ||
                        InputImage.Data[y + 1, x, 0] == 255 || InputImage.Data[y + 1, x - 1, 0] == 255)
                                Result.Data[y, x, 0] = 255;
                        else
                            Result.Data[y, x, 0] = 0;
                    }
                }
            return Result;
            }
        }
}
