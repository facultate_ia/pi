﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Emgu.CV;
using Emgu.CV.Structure;

namespace ISIP_Algorithms.Tools
{
    public class Contrast2
    {
        public static Image<Gray, byte> Contrast_Method(Image<Gray, byte> InputImage)
        {
            Image<Gray, byte> Result = new Image<Gray, byte>(InputImage.Size);
            int[] LookUp_Table = new int[256];
            double a = -2.0 / (255 * 255);
            double b = 3.0 / 255;
            for (int i = 0; i <= 255; i++)
                LookUp_Table[i] = (int)(a * Math.Pow(i,3) + b * Math.Pow(i,2)+0.5);
            for (int y = 0; y < InputImage.Height; y++)
            {
                for (int x = 0; x < InputImage.Width; x++)
                {
                    Result.Data[y, x, 0] = (byte)LookUp_Table[InputImage.Data[y,x,0]];
                }
            }
            return Result;
        }
    }
}
