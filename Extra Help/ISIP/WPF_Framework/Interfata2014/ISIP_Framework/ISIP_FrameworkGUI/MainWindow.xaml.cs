﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;

using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;
using ISIP_UserControlLibrary;

using ISIP_Algorithms.Tools;
using ISIP_FrameworkHelpers;

namespace ISIP_FrameworkGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    
    public partial class MainWindow : Window
    {

        int t = 160;

        //private Windows.Grafica dialog;
        Windows.Magnifyer MagnifWindow;
        Windows.GLine RowDisplay;

        bool Magif_SHOW = false;
        bool GL_ROW_SHOW = false;
        System.Windows.Point lastClick = new System.Windows.Point(0, 0);
        System.Windows.Point upClick = new System.Windows.Point(0, 0);

        public MainWindow()
        {
            InitializeComponent();
            mainControl.OriginalImageCanvas.MouseDown += new MouseButtonEventHandler(OriginalImageCanvas_MouseDown);
            mainControl.OriginalImageCanvas.MouseUp += new MouseButtonEventHandler(OriginalImageCanvas_MouseUp);
        }
        void OriginalImageCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            upClick = Mouse.GetPosition(mainControl.OriginalImageCanvas);
            if(Cropp_ON.IsEnabled==true)
            {
                System.Windows.Point TL = new System.Windows.Point(Math.Min(upClick.X,lastClick.X),Math.Min(upClick.Y,lastClick.Y));
                System.Windows.Point BR = new System.Windows.Point(Math.Max(upClick.X, lastClick.X), Math.Max(upClick.Y, lastClick.Y));
                if(Cropp_ON.IsChecked==true)
                {
                    DrawHelper.DrawAndGetRectangle(mainControl.OriginalImageCanvas, TL, BR, System.Windows.Media.Brushes.Red);
                    mainControl.ProcessedGrayscaleImage = Cropp.Crop(mainControl.OriginalGrayscaleImage, TL, BR);
                }
            }
        }
        void OriginalImageCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            lastClick = Mouse.GetPosition(mainControl.OriginalImageCanvas);
            DrawHelper.RemoveAllLines(mainControl.OriginalImageCanvas);
            DrawHelper.RemoveAllRectangles(mainControl.OriginalImageCanvas);
            DrawHelper.RemoveAllLines(mainControl.ProcessedImageCanvas);
            DrawHelper.RemoveAllRectangles(mainControl.ProcessedImageCanvas);
            if (GL_ROW_ON.IsChecked)
            {
                DrawHelper.DrawAndGetLine(mainControl.OriginalImageCanvas, new System.Windows.Point(0, lastClick.Y),
                     new System.Windows.Point(mainControl.OriginalImageCanvas.Width - 1, lastClick.Y), System.Windows.Media.Brushes.Red, 1);
                if (mainControl.ProcessedGrayscaleImage != null)
                {
                    DrawHelper.DrawAndGetLine(mainControl.ProcessedImageCanvas, new System.Windows.Point(0, lastClick.Y),
                     new System.Windows.Point(mainControl.ProcessedImageCanvas.Width - 1, lastClick.Y), System.Windows.Media.Brushes.Red, 1);
                }
                if (mainControl.OriginalGrayscaleImage != null) RowDisplay.Redraw((int)lastClick.Y);

            }
            if (Magnifyer_ON.IsChecked)
            {
                DrawHelper.DrawAndGetLine(mainControl.OriginalImageCanvas, new System.Windows.Point(0, lastClick.Y),
                    new System.Windows.Point(mainControl.OriginalImageCanvas.Width - 1, lastClick.Y), System.Windows.Media.Brushes.Red, 1);
                DrawHelper.DrawAndGetLine(mainControl.OriginalImageCanvas, new System.Windows.Point(lastClick.X, 0),
                    new System.Windows.Point(lastClick.X, mainControl.OriginalImageCanvas.Height - 1), System.Windows.Media.Brushes.Red, 1);
                DrawHelper.DrawAndGetRectangle(mainControl.OriginalImageCanvas, new System.Windows.Point(lastClick.X - 4, lastClick.Y - 4),
                    new System.Windows.Point(lastClick.X + 4, lastClick.Y + 4), System.Windows.Media.Brushes.Red);
                if (mainControl.ProcessedGrayscaleImage != null)
                {
                    DrawHelper.DrawAndGetLine(mainControl.ProcessedImageCanvas, new System.Windows.Point(0, lastClick.Y),
                    new System.Windows.Point(mainControl.ProcessedImageCanvas.Width - 1, lastClick.Y), System.Windows.Media.Brushes.Red, 1);
                    DrawHelper.DrawAndGetLine(mainControl.ProcessedImageCanvas, new System.Windows.Point(lastClick.X, 0),
                        new System.Windows.Point(lastClick.X, mainControl.ProcessedImageCanvas.Height - 1), System.Windows.Media.Brushes.Red, 1);
                    DrawHelper.DrawAndGetRectangle(mainControl.ProcessedImageCanvas, new System.Windows.Point(lastClick.X - 4, lastClick.Y - 4),
                        new System.Windows.Point(lastClick.X + 4, lastClick.Y + 4), System.Windows.Media.Brushes.Red);
                }
                if (mainControl.OriginalGrayscaleImage != null) MagnifWindow.RedrawMagnifyer(lastClick);
            }
        }

        private void openGrayscaleImageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            mainControl.LoadImageDialog(ImageType.Grayscale);
            Magnifyer_ON.IsEnabled = true;
            GL_ROW_ON.IsEnabled = true;
            Cropp_ON.IsEnabled = true;


        }

        private void openColorImageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            mainControl.LoadImageDialog(ImageType.Color);
            Magnifyer_ON.IsEnabled = true;
            GL_ROW_ON.IsEnabled = true;
        }

        private void saveProcessedImageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mainControl.SaveProcessedImageToDisk())
            {
                MessageBox.Show("Processed image not available!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void saveAsOriginalMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.ProcessedGrayscaleImage != null)
            {
                mainControl.OriginalGrayscaleImage = mainControl.ProcessedGrayscaleImage;
            }
            else if(mainControl.ProcessedColorImage != null)
            {
                mainControl.OriginalColorImage = mainControl.ProcessedColorImage;
            }
        }

        private void Invert_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {

                mainControl.ProcessedGrayscaleImage=Tools.Invert(mainControl.OriginalGrayscaleImage);
            }

        }

        private void Magnifyer_ON_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                if (Magif_SHOW == true)
                {
                    Magif_SHOW = false;
                    MagnifWindow.Close();
                    DrawHelper.RemoveAllLines(mainControl.OriginalImageCanvas);
                    DrawHelper.RemoveAllRectangles(mainControl.OriginalImageCanvas);
                    DrawHelper.RemoveAllLines(mainControl.ProcessedImageCanvas);
                    DrawHelper.RemoveAllRectangles(mainControl.ProcessedImageCanvas);

                }
                else Magif_SHOW = true;
                if (Magif_SHOW == true)
                {
                    MagnifWindow = new Windows.Magnifyer(mainControl.OriginalGrayscaleImage, mainControl.ProcessedGrayscaleImage);
                    MagnifWindow.Show();
                    MagnifWindow.RedrawMagnifyer(lastClick);
                }
            }

        }

        private void Crop_ON_Click(object sender, RoutedEventArgs e)
        {

        }
        private void GL_ROW_ON_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                if (GL_ROW_SHOW == true)
                {
                    GL_ROW_SHOW = false;
                    RowDisplay.Close();
                    DrawHelper.RemoveAllLines(mainControl.OriginalImageCanvas);
                    DrawHelper.RemoveAllLines(mainControl.ProcessedImageCanvas);
                }
                else GL_ROW_SHOW = true;

                if (GL_ROW_SHOW == true)
                {
                    RowDisplay = new Windows.GLine(mainControl.OriginalGrayscaleImage, mainControl.ProcessedGrayscaleImage);

                    RowDisplay.Show();
                    RowDisplay.Redraw((int)lastClick.Y);

                }
            }
        }

        private void Binarizare_Click(object sender, RoutedEventArgs e)
        {
            UserInputDialog dlg = new UserInputDialog("Binar", new string[] { "t=" });
            if(mainControl.OriginalGrayscaleImage!=null)
            {
                if(dlg.ShowDialog().Value==true)
                {
                    mainControl.ProcessedGrayscaleImage = Binar.Binarizare(mainControl.OriginalGrayscaleImage, (int)dlg.Values[0]);
                }
            }
        }

        private void Flip_Click(object sender, RoutedEventArgs e)
        {
            if(mainControl.OriginalGrayscaleImage!=null)
            {
                mainControl.ProcessedGrayscaleImage = Flip.Flipp(mainControl.OriginalGrayscaleImage);
            }
        }

        
        private void COSINE_CLICK(object sender, RoutedEventArgs e)
        {
            Windows.Canvas cnv = new Windows.Canvas();
            cnv.Show();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Binarizare2_Click(object sender, RoutedEventArgs e)
        {
            UserInputDialog dlg = new UserInputDialog("Binar", new string[] { "t1=", "t2=" });
            if (mainControl.OriginalGrayscaleImage != null)
            {
                if (dlg.ShowDialog().Value == true)
                {
                    mainControl.ProcessedGrayscaleImage = Binar2.Binarizare2(mainControl.OriginalGrayscaleImage, (int)dlg.Values[0],(int)dlg.Values[1]);
                }
            }
        }

        private void CONTRAST2_CLICK(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                    mainControl.ProcessedGrayscaleImage = Contrast2.Contrast_Method(mainControl.OriginalGrayscaleImage);
            }
        }

        private void threeXthree_click(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                mainControl.ProcessedGrayscaleImage = ThreeXThree.Three_Method(mainControl.OriginalGrayscaleImage);
            }
        }

        private void Round_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                mainControl.ProcessedGrayscaleImage = Round.Round_Method(mainControl.OriginalGrayscaleImage);
            }
        }

        private void ZeroCrossing_Click(object sender, RoutedEventArgs e)
        {
            mainControl.OriginalGrayscaleImage = Zero.Function(768, 576);
            UserInputDialog dlg = new UserInputDialog("Binar", new string[] { "threshold=" });
            if (dlg.ShowDialog().Value == true)
                mainControl.ProcessedGrayscaleImage = Zero.Zero_Method(768, 576,(int)dlg.Values[0]);
        }

        private void Dilation3x3(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                mainControl.OriginalGrayscaleImage = Binar.Binarizare(mainControl.OriginalGrayscaleImage, t);
                mainControl.ProcessedGrayscaleImage = Dilation3X3.Dilation3X3_Method(mainControl.OriginalGrayscaleImage);
            }
        }

        private void Dilation1x3(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                mainControl.OriginalGrayscaleImage = Binar.Binarizare(mainControl.OriginalGrayscaleImage, t);
                mainControl.ProcessedGrayscaleImage = Dilation1X3.Dilation1X3_Method(mainControl.OriginalGrayscaleImage);
            }
        }

        private void Dilation3x1(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                mainControl.OriginalGrayscaleImage = Binar.Binarizare(mainControl.OriginalGrayscaleImage, t);
                mainControl.ProcessedGrayscaleImage = Dilation3X1.Dilation3X1_Method(mainControl.OriginalGrayscaleImage);
            }
        }

        private void Erosion3x3(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                mainControl.OriginalGrayscaleImage = Binar.Binarizare(mainControl.OriginalGrayscaleImage, t);
                mainControl.ProcessedGrayscaleImage = Erotion3X3.Erotion3X3_Method(mainControl.OriginalGrayscaleImage);
            }
        }

        private void Erosion1x3(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                mainControl.OriginalGrayscaleImage = Binar.Binarizare(mainControl.OriginalGrayscaleImage, t);
                mainControl.ProcessedGrayscaleImage = Erotion1X3.Erotion1X3_Method(mainControl.OriginalGrayscaleImage);
            }
        }

        private void Erosion3x1(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                mainControl.OriginalGrayscaleImage = Binar.Binarizare(mainControl.OriginalGrayscaleImage, t);
                mainControl.ProcessedGrayscaleImage = Erotion3X1.Erotion3X1_Method(mainControl.OriginalGrayscaleImage);
            }
        }

        private void Rotation_Click(object sender, RoutedEventArgs e)
        {
            UserInputDialog dlg = new UserInputDialog("Rotation", new string[] { "alfa=" });
            if (mainControl.OriginalGrayscaleImage != null)
            {
                if (dlg.ShowDialog().Value == true)
                    mainControl.ProcessedGrayscaleImage = Rotation_Function.Rotation_Method(mainControl.OriginalGrayscaleImage, (int)dlg.Values[0]);
            }
        }

        private void CRectangle_Click(object sender, RoutedEventArgs e)
        {
            if (mainControl.OriginalGrayscaleImage != null)
            {
                List<System.Windows.Point> maxime;
                mainControl.ProcessedGrayscaleImage = Rectangle_Function.Rectangle_Method(mainControl.OriginalGrayscaleImage,out maxime,mainControl.OriginalImageCanvas);
                //for (int i = 0; i < maxime.Count-1; i++)
                //    DrawHelper.DrawAndGetLine(mainControl.OriginalImageCanvas,maxime[i],maxime[i+1], System.Windows.Media.Brushes.Red);


            }
        }

        private void Sobel_Click(object sender, RoutedEventArgs e)
        {
            UserInputDialog dlg = new UserInputDialog("Rotation", new string[] { "T=" });
            if (mainControl.OriginalGrayscaleImage != null)
            {
                if (dlg.ShowDialog().Value == true)
                    mainControl.ProcessedGrayscaleImage = Sobel.Sobel_Method(mainControl.OriginalGrayscaleImage, (int)dlg.Values[0]);
            }
        }

        private void Runes_Click(object sender, RoutedEventArgs e)
        {
            UserInputDialog dlg = new UserInputDialog("Binar", new string[] { "t=" });
            if (mainControl.OriginalGrayscaleImage != null)
            {
                if (dlg.ShowDialog().Value == true)
                {
                    mainControl.OriginalGrayscaleImage = Binar.Binarizare(mainControl.OriginalGrayscaleImage, (int)dlg.Values[0]);
                    mainControl.ProcessedGrayscaleImage = CCL_Function.CCL_Method(mainControl.OriginalGrayscaleImage);
                }
            }
        }

        private void mainControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
