﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using ISIP_FrameworkHelpers;

namespace ISIP_FrameworkGUI.Windows
{
    /// <summary>
    /// Interaction logic for Canvas.xaml
    /// </summary>
    public partial class Canvas : Window
    {
        public Canvas()
        {
            InitializeComponent();
        }

        public void DrawGraph()
        {
            List<Point> puncte = new List<Point>();

            int k;

            Point y1 = new Point(GraphCanvas.ActualWidth / 2, 10);
            Point y2 = new Point(GraphCanvas.ActualWidth / 2, GraphCanvas.ActualHeight - 10);
            Point x1 = new Point(10, GraphCanvas.ActualHeight / 2);
            Point x2 = new Point(GraphCanvas.ActualWidth - 10, GraphCanvas.ActualHeight / 2);
            DrawHelper.DrawAndGetLine(GraphCanvas, y1, y2, Brushes.Blue);
            DrawHelper.DrawAndGetLine(GraphCanvas, x1, x2, Brushes.Blue);
            DrawHelper.DrawText(GraphCanvas, "x", new Point(GraphCanvas.ActualWidth - 9, GraphCanvas.ActualHeight / 2), 12, Colors.Black);
            DrawHelper.DrawText(GraphCanvas, "f(x)", new Point(GraphCanvas.ActualWidth / 2 - 20, 9), 12, Colors.Black);
            DrawHelper.DrawText(GraphCanvas, "0", new Point(GraphCanvas.ActualWidth/2, GraphCanvas.ActualHeight / 2), 12, Colors.Black);
            DrawHelper.DrawText(GraphCanvas, "-pi", new Point(-Math.PI+150, GraphCanvas.ActualHeight / 2), 12, Colors.Black);
            DrawHelper.DrawText(GraphCanvas, "pi", new Point(Math.PI+450,GraphCanvas.ActualHeight/2), 12, Colors.Black);
            
            try
            {
                System.Exception error = new System.Exception();
                k = int.Parse(K.Text);
                if (k <= 0 || k.GetType() != typeof(int))
                    throw error;
                for (double x = -Math.PI; x <= Math.PI; x=x+Math.PI/180)
                {
                    double y = k * Math.Cos(x);
                    Point p = new Point(100*x+GraphCanvas.ActualWidth/2, GraphCanvas.ActualHeight/2- y);
                    puncte.Add(p);
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }
            DrawHelper.DrawAndGetPolyline(GraphCanvas, puncte, Brushes.Red);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GraphCanvas.Children.Clear();
            DrawGraph();
        }
    }
}
