﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using Emgu.CV;
using Emgu.CV.Structure;

namespace ISIP_Algorithms.Tools
{
    public class Cropp
    {
        public static Image<Gray,byte> Crop(Image<Gray,byte> InputImage,System.Windows.Point TL,System.Windows.Point BR)
        {
            int width, height;
            width = (int)(BR.X - TL.X +1);
            height = (int)(-TL.Y + BR.Y +1);
            Image<Gray, byte> Result = new Image<Gray, byte>(width,height);
            for(int y=0;y<Result.Height;y++)
            {
                for (int x = 0; x < Result.Width; x++)
                    Result.Data[y, x, 0] = InputImage.Data[y + (int)(TL.Y), x + (int)(TL.X), 0];
            }
            return Result;    
        }
    }
}
