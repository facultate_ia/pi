from Application.Utils.AlgorithmDecorators import RegisterAlgorithm
from Application.Utils.InputDecorators import InputDialog
from Application.Utils.OutputDecorators import OutputDialog

import numpy as np
import math

@RegisterAlgorithm("Twirl", "Geometric Transformations")
@InputDialog(alpha=float, r_max=float)
@OutputDialog(title="Twirl")
def erosion(image, alpha, r_max):
    """Applies twirl transformation.
    :param image:
    :param alpha:
    :param r_max:
    :return:
    """

    alpha = alpha*(np.pi/180)

    x_0 = image.shape[0] // 2
    y_0 = image.shape[1] // 2

    processed_image = image.copy()

    for x in range(image.shape[0]):
        for y in range(image.shape[1]):
            d_x = x - x_0
            d_y = y - y_0

            r = np.sqrt(d_x**2 + d_y**2)

            beta = math.atan2(d_y, d_x) + alpha*((r_max-r)/r_max)

            x_c = int(x_0 + r*np.cos(beta) if r<=r_max else x)
            y_c = int(y_0 + r*np.sin(beta) if r<=r_max else y)

            processed_image[x,y] = image[x_c, y_c]

    return {
        'processedImage': processed_image
    }