from Application.Utils.AlgorithmDecorators import RegisterAlgorithm
from Application.Utils.InputDecorators import InputDialog
from Application.Utils.OutputDecorators import OutputDialog
from Application.ImageProcessingAlgorithms.Filters import sobel_filter

import numpy as np
import math
import cv2

@RegisterAlgorithm("Hough Transform 3", "Segmentation")
@OutputDialog(title="HoughTransf3")
def hough_transform(image):
    """Applies Hough transformation using 3 cadrans.
    :param image:
    :return:
    """

    original_image = image.copy()

    image = sobel_filter(image)['processedImage']

    hough_matrix_dimensions = (int(math.sqrt(image.shape[0]**2 + image.shape[1]**2)), 271)
    hough_matrix = np.zeros(shape=hough_matrix_dimensions)
    representation = hough_matrix.copy()
    maxim = 0

    for x in range(0, image.shape[0]):
        for y in range(0, image.shape[1]):
            if image[x, y] == 255:
                for theta in range(-90, 181):
                    r = x * math.cos(theta * np.pi/180) + y * math.sin(theta * np.pi/180)
                    if r >= 0:
                        hough_matrix[round(r), theta + 90] += 1
                        if hough_matrix[round(r), theta + 90] > maxim:
                            maxim = hough_matrix[round(r), theta + 90]

    for x in range(hough_matrix.shape[0]):
        for y in range(hough_matrix.shape[1]):
            representation[x, y] = int((255.0 / maxim) * hough_matrix[x, y])

    processed_image = representation

    for r in range(0, representation.shape[0]):
        for theta in range (0, representation.shape[1]):
            if representation[r, theta] > 150:
                x1 = 0
                x2 = 0
                y1 = 0
                y2 = image.shape[1] - 1
                if theta - 90 == 0 or theta - 90 == 180:
                    x1 = r
                    x2 = r
                else:
                    x1 = int(r / math.sin(np.pi / 180 * (theta - 90)))
                    x2 = int((r - y2 * math.cos(np.pi / 180 * (theta - 90))) / math.sin(np.pi / 180 * (theta - 90)))
                original_image = cv2.line(original_image,(x1,y1),(x2,y2),(255,0,0),3)

    return {
        'originalImage': original_image,
        'processedImage': processed_image.astype(np.uint8)
    }