from Application.Utils.AlgorithmDecorators import RegisterAlgorithm
from Application.Utils.InputDecorators import InputDialog
from Application.Utils.OutputDecorators import OutputDialog

from numpy import math
import numpy as np
from scipy import ndimage
from scipy.ndimage.filters import convolve

@RegisterAlgorithm("Sobel filter", "Filters")
@InputDialog(threshold=int)
def sobel_filter(image, threshold):

    Sx = np.array([(-1,0,1),(-2,0,2),(-1,0,1)])
    Sy = np.array([(-1,-2,-1), (0,0,0), (1,2,1)])

    processed_image = np.zeros((image.shape[0],image.shape[1]))

    for i in range(1,image.shape[0]-1):
        for j in range(1,image.shape[1]-1):
            newMatrix = image[i-1:i+2,j-1:j+2]
            fx = (newMatrix * Sx).sum()
            fy = (newMatrix * Sy).sum()
            grade = np.sqrt(fx**2 + fy**2)

            if(grade >= threshold):
                radians = math.atan2(fx,fy)
                grades = math.degrees(radians)
                processed_image[i,j] = 255
            else:
                processed_image[i,j] = 0
    return {
        'processedImage': processed_image.astype(np.uint8)
    }

@RegisterAlgorithm("Bilateral filter", "Filters")
@InputDialog(sigma_d=float, sigma_r=float)
@OutputDialog(title="Bilateral filter")
def bilateral_filter(image, sigma_d, sigma_r):
    """Applies a bilateral filter to the image.
    :param image:
    :param sigma_d:
    :param sigma_r:
    :return:
    """

    dim = int(np.round(4 * sigma_d) if np.round(4 * sigma_d) % 2 == 1 else np.round(4 * sigma_d) + 1)
    h_d = np.ndarray(shape=(dim,dim))

    for i in range(0, dim):
        for j in range(0, dim):
            h_d[i,j] = np.e ** (-(i ** 2 + j ** 2) / (2 * sigma_d ** 2))

    border_dim = int(dim / 2)
    for x in range(border_dim, image.shape[0] - border_dim):
        for y in range(border_dim, image.shape[1] - border_dim):
            print("Iteration: " + str(x) + "," + str(y))
            sum_sus = 0
            sum_jos = 0
            for i in range(-border_dim, border_dim+1):
                for j in range(-border_dim, border_dim+1):
                    h_r = np.e ** (-((int(image[x+i,y+j]) - int(image[x,y])) ** 2) / (2 * sigma_r ** 2))
                    sum_sus += image[x+i,y+j] * h_d[i,j] * h_r
                    sum_jos += h_d[i,j] * h_r
            image[x,y] = sum_sus / sum_jos

    return {
        'processedImage': image
    }

def gaussian_filter(image, sigma = 1):
    
    dim_masca = int(np.round(4 * sigma) if np.round(4 * sigma) % 2 == 1 else np.round(4 * sigma) + 1)
    k = int(dim_masca / 2)
    constanta = 1 / (2 * np.pi * sigma**2)
    
    G = np.ndarray(shape = (dim_masca, dim_masca))
    sum = 0
    for i in range(-k, k + 1):
        for j in range(-k, k + 1):
                G[i+k,j+k] = constanta * np.e**(-((i**2+j**2)/(2*(sigma**2))))
                sum += G[i+k,j+k]

    G = G/sum

    for x in range(k, image.shape[0] - k):
        for y in range(k, image.shape[1]- k):
                sum = 0
                for i in range(-k, k + 1):
                        for j in range(-k, k + 1):
                                sum = sum + (G[i + k, j + k] * image[x + i, y + j])
                image[x,y] = sum
    return image.astype(np.uint8)

def sobel_gradient(image, T1):
    
    Sx = np.array([(-1,0,1),(-2,0,2),(-1,0,1)])
    Sy = np.array([(-1,-2,-1), (0,0,0), (1,2,1)])

    Grad = np.zeros(shape=image.shape)

    for i in range(1,image.shape[0]-1):
        for j in range(1,image.shape[1]-1):
            newMatrix = image[i-1:i+2,j-1:j+2]
            fx = (newMatrix * Sx).sum()
            fy = (newMatrix * Sy).sum()
            grade = np.sqrt(fx**2 + fy**2)

            if(grade >= T1):
                radians = math.atan2(fy,fx)
                theta = math.degrees(radians)
                # Grad[i,j] = 255
                if -22.5 <= theta < 22.5 or -180 <= theta < -157.5 or 157.5 <= theta < 180:
                    Grad[i,j] = 50
                elif 22.5 <= theta < 67.5 or -157.5 <= theta < -112.5:
                    Grad[i,j] = 100
                elif 67.5 <= theta < 112.5 or -112.5 <= theta < -67.5:
                    Grad[i,j] = 150
                elif 112.5 <= theta < 157.5 or -67.5 <= theta < -22.5:
                    Grad[i,j] = 250
            else:
                Grad[i,j] = 0

    return Grad.astype(np.uint8), theta

def non_max_suppression(image, theta):
    img_final = np.zeros(image.shape, dtype=int)
    angle = theta * 180.0 / np.pi
    angle += 180

    for i in range(1, image.shape[0] - 1):
        for j in range(1, image.shape[1] - 1):
            q = 255
            r = 255

            if (-22.5 <= angle < 22.5):
                q = image[i, j+1]
                r = image[i, j-1]

            elif (22.5 <= angle < 67.5):
                q = image[i+1, j-1]
                r = image[i-1, j+1]

            elif (67.5 <= angle < 90) or (-67.5 <= angle < -90):
                q = image[i+1, j]
                r = image[i-1, j]

            elif (-22.5 <= angle < -67.5):
                q = image[i-1, j-1]
                r = image[i+1, j+1]

            if (image[i,j] >= q) and (image[i,j] >= r):
                img_final[i,j] = image[i,j]
            else:
                img_final[i,j] = 0

    return img_final.astype(np.uint8)

def hysteresys_thresholding(image, T1, T2):
    image[image <= T1] = 0
    image[image > T2] = 255

    queue = [(int, int)]

    for x in range(0, image.shape[0]):
        for y in range(0, image.shape[1]):
            if image[x,y] == 255:
                queue.append((x,y))

    print(type(queue[0]))

    for index in range(0, len(queue)):
        for i in range(-1, 1):
            for j in range(-1, 1):
                if image[queue[index][0]+i, queue[index][1]+j] > T1 & image[queue[index][0]+i, queue[index][1]+j] <= T2:
                    image[index[0]+i,index[1]+j] = 255
                    queue.append((int(queue[index][0]+i), int(queue[index][1]+j)))

    return image.astype(np.uint8)

@RegisterAlgorithm("Canny filter", "Filters")
@InputDialog(T1=int, T2=int)
@OutputDialog(title="Canny filter")
def canny_filter(image, T1, T2):
    """Applies a canny filter to the image.
    :param image:
    :param T1:
    :param T2:
    :return:
    """
    
    image = gaussian_filter(image)

    image, theta = sobel_gradient(image, T1)

    # image = non_max_suppression(image, theta)

    # image = hysteresys_thresholding(image, T1, T2)

    return {
        'processedImage': image
    }

