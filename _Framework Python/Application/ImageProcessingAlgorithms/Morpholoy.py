from Application.Utils.AlgorithmDecorators import RegisterAlgorithm
from Application.Utils.InputDecorators import InputDialog
from Application.Utils.OutputDecorators import OutputDialog

import numpy as np

@RegisterAlgorithm("Erosion", "Morphology")
def erosion(image, s=7, t=4):
    """Applies erosion.
    :param image:
    :return:
    """

    eroded_img = image.copy()

    s = s//2
    t = t//2

    for x in range(s, image.shape[0] - s):
        for y in range(t, image.shape[1] - t):
            if image[x,y] == 0:
                eroded_img[x,y] = 0
            else:
                isPixel0 = False
                for i in range(-s, s):
                    for j in range(-t, t):
                            if image[x+i, y+j] == 0 :
                                eroded_img[x,y] = 0
                                isPixel0 = True
                                break
                    if isPixel0 == True:
                        break
    return {
        'processedImage' : eroded_img
    }


@RegisterAlgorithm("XOR", "Morphology")
def XOR_Morphology(image):
    """Applies XOR between a binary image and eroted image.
    :param image:
    :return:
    """

    eroded_img = erosion(image, 7, 4)['processedImage']

    processed_img = image

    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):
            if image[i, j] == eroded_img[i, j]:
                processed_img[i, j] = 0
            else:
                processed_img[i, j] = 255

    # processed_img[processed_img == eroded_img] = 0
    # processed_img[processed_img != eroded_img] = 255

    return {
        'processedImage': processed_img
    }