"""
Module docstring?
"""
import numpy as np

from Application.Utils.AlgorithmDecorators import RegisterAlgorithm
from Application.Utils.InputDecorators import InputDialog
from Application.Utils.OutputDecorators import OutputDialog


@RegisterAlgorithm("Invert", "PointwiseOp")
def invert(image):
    """Inverts every pixel of the image.

    :param image:
    :return:
    """
    return {
        'processedImage': np.invert(image)
    }


@RegisterAlgorithm("Flip LR", "PointwiseOp")
def flip_lr(image):
    """Flips the image left to right.

    :param image:
    :return:
    """
    return {
        'processedImage': np.flip(image, axis=1)
    }


@RegisterAlgorithm("Flip UD", "PointwiseOp")
def flip_ud(image):
    """Flips the image upside down.

    :param image:
    :return:
    """
    return {
        'processedImage': np.flip(image, axis=0)
    }

def LUT_method(value, LUT):
    return LUT[value]

@RegisterAlgorithm("Piecewise Linear contrast", "PointwiseOp")
@InputDialog(r1=int, r2=int, s1=int, s2=int)
@OutputDialog(title="Piecewise Linear Output")
def piecewise_linear(image, r1, r2, s1, s2):
    """
    Calculates the Piecewise Linear contrast

    :param image:
    :return:
    """

    alpha = s1/r1
    beta = (s2 - s1)/(r2 - r1)
    gamma = (255 - s2)/(255 - r2)

    LUT = np.ndarray(shape=(256), dtype=np.uint8)

    for r in range(0, r1):
        LUT[r] = round(alpha * r + 0.5)

    for r in range(r1, r2):
        LUT[r] = round(beta * (r - r1) + s1 + 0.5)

    for r in range(r2, 255):
        LUT[r] = round(gamma * (r - r2) + s2 + 0.5)
    
    # for line in range(image.shape[1]):
    #     for column in range(image.shape[0]):
    #         image[line][column] = LUT[image[line][column]]

    image = LUT_method(image, LUT)

    return {
        'processedImage': image
    }