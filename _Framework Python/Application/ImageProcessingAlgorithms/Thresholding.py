from Application.Utils.AlgorithmDecorators import RegisterAlgorithm
from Application.Utils.InputDecorators import InputDialog
from Application.Utils.OutputDecorators import OutputDialog

import numpy as np

@RegisterAlgorithm("Binarization", "Thresholding")
@InputDialog(threshold=int)
@OutputDialog(title="Binarization Output")
def binarization(image, threshold):
    """Applies binarization based on given threshold.

    :param image:
    :param threshold:
    :return:
    """
    # if the image is grayscale
    if len(image.shape) == 2:
        image[image < threshold] = 0
        image[image >= threshold] = 255
        return {
            'processedImage': image
        }
    else:
        return {
            'outputMessage': "Error: image is not grayscale!"
        }


@RegisterAlgorithm("Binarization with two thresholds", "Thresholding")
@InputDialog(threshold1=int, threshold2=int)
@OutputDialog(title="Binarization Output")
def binarization2(image, threshold1, threshold2):
    """Applies binarization based on given thresholds.

    :param image:
    :param threshold1:
    :param threshold2:
    :return:
    """
    # if the image is grayscale
    if len(image.shape) == 2:
        image[np.logical_or(threshold1 > image, image > threshold2)] = 0
        image[np.logical_and(threshold1 <= image, image <= threshold2)] = 255
        return {
            'processedImage': image
        }
    else:
        return {
            'outputMessage': "Error: image is not grayscale!"
        }

@RegisterAlgorithm("Intermeans Binarization", "Thresholding")
@OutputDialog(title="Intermeans Binarization Output")
def intermeans(image):
    """Applies intermeans binarization based on given thresholds.

    :param image:
    :param threshold:
    :return:
    """
    threshold0 = (int(image.max()) + int(image.min())) / 2

    default_difference = 1

    threshold = threshold0

    C1 = image[image > threshold]
    C2 = image[image <= threshold]

    mu1 = C1.mean()
    mu2 = C2.mean()

    threshold = round((mu1 + mu2) / 2)

    while threshold - threshold0 >= default_difference:
        threshold0 = threshold

        C1 = image[image > threshold]
        C2 = image[image <= threshold]

        mu1 = C1.mean()
        mu2 = C2.mean()

        threshold = round((mu1 + mu2) / 2)

    print()
    print("Intermeans threshold: ")
    print(threshold)

    if len(image.shape) == 2:
        image[image < threshold] = 0
        image[image >= threshold] = 255
        return {
            'processedImage': image
        }
    else:
        return {
            'outputMessage': "Error: image is not grayscale!"
        }